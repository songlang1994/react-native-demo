// @flow

export function URLJoin(parts: string[], params?: { [k: string]: string }) {
  const queries = []
  if (params) {
    for (const k in params) {
      queries.push(`${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
    }
    return parts.join('/') + '?' + queries.join('&')
  }
  return parts.join('/')
}
