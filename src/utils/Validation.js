// @flow

export { default as isEmail } from 'validator/lib/isEmail'

export const isPassword = validateWithRegex(/(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(?=.{8,})/)

function validateWithRegex(regex: RegExp) {
  return (str: string) => regex.test(str)
}
