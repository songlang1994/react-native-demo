export const turnHorizontal = {
  style: {
    perspective: 1000
  },
  '0': {
    rotateY: '0deg'
  },
  '0.499': {
    rotateY: '90deg'
  },
  '0.5': {
    rotateY: '-90deg'
  },
  '1': {
    rotateY: '0deg'
  }
}
