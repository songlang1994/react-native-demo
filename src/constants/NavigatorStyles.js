// @flow

import * as C from './Colors'

export const DEFAULT_STYLE = {
  navBarHidden       : true            ,
  drawUnderStatusBar : true            ,
  statusBarColor     : C.STATUS_BAR_BG ,
}
