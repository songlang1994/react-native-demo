// @flow

export const LIGHT             = '#FFF'
export const LIGHT_TRANSLUCENT = 'rgba(255, 255, 255, 0.5)'
export const DARK              = '#000'
export const DARK_TRANSLUCENT  = 'rgba(0, 0, 0, 0.5)'

export const STATUS_BAR_BG     = 'rgba(0, 0, 0, 0.4)'
export const TEXT_LIGHT        = '#CCC'
export const TEXT_DARK         = '#333'
export const TEXT_PLACEHOLDER  = '#DDD'
