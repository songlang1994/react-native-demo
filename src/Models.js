// @flow

export type User = {
  id                   : number  ,
  email                : string  ,
  password             : string  ,
  createdAt            : Date    ,
  updatedAt            : Date    ,
  accessToken          : string  ,
  refreshToken         : string  ,
  passwordDigest       : string  ,
  accessTokenCreatedAt : Date    ,
  admin                : boolean ,
  digitalKeyId         : number  ,
  wubookMail           : string  ,
  superAdmin           : boolean ,
  deviceId             : number  ,
  platform             : string  ,
}
