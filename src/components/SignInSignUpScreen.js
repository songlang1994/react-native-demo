// @flow

import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TextInput,
  TouchableHighlight,
  ToastAndroid
} from 'react-native'
import { Navigation } from 'react-native-navigation'
import * as Animatable from 'react-native-animatable'
import ScreenView from './layouts/ScreenView'
import TouchableText from './common/TouchableText'
import * as C from '../constants/Colors'
import { DEFAULT_STYLE } from '../constants/NavigatorStyles'
import { login } from '../services/SessionService'
import { register } from '../services/UserService'
import Screens from './Screens'
import AppState from '../AppState'
import * as Validation from '../utils/Validation'

type State = {
  isSignIn: boolean,
  isAnimating: boolean
}

export default class SignInSignUpScreen extends React.PureComponent<any, State> {

  static +navigatorStyle = DEFAULT_STYLE

  // $FlowFixMe
  _emailInputRef = React.createRef()
  // $FlowFixMe
  _passwordInputRef = React.createRef()
  // $FlowFixMe
  _inputGroupRef = React.createRef()

  state = {
    isSignIn: true,
    isAnimating: false
  }

  render() {
    return (
      <ScreenView backgroundImage={require('../assets/LandingBackBlur.jpg')}>
        <TouchableText style={S.changeMode}
                       onPress={this._onChangeModePressHandler}>
          {this.state.isSignIn ? 'Sign Up' : 'Sign In' }
        </TouchableText>
        <View style={S.root}>
          <View style={S.contentContainer}>
            <Animatable.View ref={this._inputGroupRef}
                             useNativeDriver={true}
                             direction={this.state.isSignIn ? 'normal' : 'reverse'}
                             easing="linear">
              <View style={[S.inputContainer, S.inputContainerTop]}>
                <Text style={S.icon}>&#xE013;</Text>
                <TextInput style={S.input}
                          ref={this._emailInputRef}
                          returnKeyType="next"
                          placeholder="Email"
                          placeholderTextColor={C.TEXT_PLACEHOLDER}
                          textContentType="emailAddress"
                          keyboardType="email-address"
                          underlineColorAndroid="transparent" />
              </View>
              <View style={[S.inputContainer, S.inputContainerBottom]}>
                <Text style={S.icon}>&#xE014;</Text>
                <TextInput style={S.input}
                          ref={this._passwordInputRef}
                          returnKeyType="go"
                          placeholder="Password"
                          placeholderTextColor={C.TEXT_PLACEHOLDER}
                          secureTextEntry={true}
                          textContentType="password"
                          underlineColorAndroid="transparent" />
              </View>
            </Animatable.View>
            <TouchableText style={[S.actionButton, this._actionButtonVisibility]}
                           textSize="Medium"
                           onPress={this._onActionButtonPressHandler}>
              {this.state.isSignIn ? 'Sign In' : 'Sign Up' }
            </TouchableText>
            <TouchableText style={[S.forgotPassword, this._forgotPasswordVisibility]}
                           onPress={this._onForgorPasswordPressHandler}>
              Forgot password?
            </TouchableText>
          </View>
        </View>
      </ScreenView>
    )
  }

  get _actionButtonVisibility() {
    return this.state.isAnimating ? { display: 'none' } : null
  }

  get _forgotPasswordVisibility() {
    return !this.state.isSignIn || this.state.isAnimating ? { display: 'none' } : null
  }

  async _animShakeInputGroup() {
    return this._inputGroupRef.current.shake(500)
  }

  async _animTurnOverInputGroup() {
    return this._inputGroupRef.current.turnHorizontal(300)
  }

  _onChangeModePressHandler = () => {
    this.setState(state => {
      return {
        isSignIn: !state.isSignIn,
        isAnimating: true
      }
    }, async () => {
      await this._animTurnOverInputGroup()
      this.setState({ isAnimating: false })
    })
  }

  _onActionButtonPressHandler = async () => {
    const email: string = this._emailInputRef.current._lastNativeText || ''
    const password: string = this._passwordInputRef.current._lastNativeText || ''

    if (!Validation.isEmail(email) || !Validation.isPassword(password)) {
      ToastAndroid.show('Input format not acceptable.', ToastAndroid.SHORT)
      this._animShakeInputGroup()
      return
    }

    const action = this.state.isSignIn ? login : register
    const result = await action({ email, password })
    if (result.success) {
      AppState.user = result.data
      Navigation.showModal({
        screen: Screens.BookingScreen
      })
    } else {
      ToastAndroid.show(result.code, ToastAndroid.SHORT)
      this._animShakeInputGroup()
    }
  }

  _onForgorPasswordPressHandler = () => {
    Navigation.showModal({
      screen: Screens.ForgotPasswordScreen
    })
  }
}

const S = StyleSheet.create({
  changeMode: {
    marginRight: 10,
    alignSelf: 'flex-end'
  },

  root: {
    flex: 1,
    justifyContent: 'center'
  },

  contentContainer: {
    alignItems: 'stretch',
    height: 350,
    padding: 10
  },

  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: C.LIGHT
  },

  inputContainerTop: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },

  inputContainerBottom: {
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },

  icon: {
    fontFamily: 'jokkspot',
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10
  },

  input: {
    flex: 1,
    fontSize: 18
  },

  actionButton: {
    height: 50,
    borderWidth: 1,
    borderColor: C.LIGHT_TRANSLUCENT,
    borderRadius: 3,
    marginTop: 15
  },

  forgotPassword: {
    marginTop: 20,
    alignSelf: 'center'
  }
})
