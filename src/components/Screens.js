// @flow

import { Navigation } from 'react-native-navigation'

import SignInSignUpScreen from './SignInSignUpScreen'
import ForgotPasswordScreen from './ForgotPasswordScreen'
import BookingScreen from './BookingScreen'

// Add screens here to registry them to react native navigation
const screensMap = {
  SignInSignUpScreen,
  ForgotPasswordScreen,
  BookingScreen
}

const screenNamesMap = {}

for(const s in screensMap) {
  Navigation.registerComponent(s, () => screensMap[s])
  screenNamesMap[s] = s
}

export default ((screenNamesMap: any) : $ObjMap<typeof screensMap, any => string>)
