import React from 'react'
import { Text } from 'react-native'
import ScreenView from './ScreenView'
import renderer from 'react-test-renderer'

describe('ScreenView', () => {
  describe('renders an ImageBackground as root when', () => {
    it('passed `backgroundImage` property', () => {
      const tree = renderer
        .create(<ScreenView backgroundImage={require('../assets/LandingBackBlur.jpg')} />)
        .toJSON()
      expect(tree).toMatchSnapshot()
    })
  })

  describe('renders an View as root when', () => {
    it('did NOT pass `backgroundImage` property', () => {
      const tree = renderer
        .create(<ScreenView />)
        .toJSON()
      expect(tree).toMatchSnapshot()
    })
  })

  it('renders all child nodes passed in', () => {
    const tree = renderer
      .create(
        <ScreenView>
          <Text>MIAO</Text>
          <Text>A</Text>
        </ScreenView>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('composes the styles passed in', () => {
    const tree = renderer
      .create(
        <ScreenView styles={{
          backgroundColor: 'white',
          padding: 10
        }} />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
