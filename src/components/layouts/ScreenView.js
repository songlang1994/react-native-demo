// @flow

import React from 'react'
import {
  Platform,
  StyleSheet,
  StatusBar,
  View,
  ImageBackground
} from 'react-native'
import { Navigation } from 'react-native-navigation'

import type { ViewStyleProp } from 'StyleSheet'

type Props = {
  backgroundColor?: string,
  backgroundImage?: number,
  style?: ViewStyleProp,
  children?: React$Node
}

export default class ScreenView extends React.PureComponent<Props> {

  render() {
    const style = [S.main, this.props.style]

    if (this.props.backgroundImage) {
      return (
        <ImageBackground source={this.props.backgroundImage} style={style}>
          {this.props.children}
        </ImageBackground>
      )
    } else {
      return (
        <View style={style}>
          {this.props.children}
        </View>
      )
    }
  }
}

const S = StyleSheet.create({
  main: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    paddingTop: Platform.select({ ios: 0, android: StatusBar.currentHeight })
  }
})
