// @flow

import React from 'react'
import {
  StyleSheet,
  Text
} from 'react-native'
import { Navigation } from 'react-native-navigation'
import ScreenView from './layouts/ScreenView'
import { DEFAULT_STYLE } from '../constants/NavigatorStyles'

export default class BookingScreen extends React.PureComponent<void> {

  static +navigatorStyle = DEFAULT_STYLE

  render() {
    return (
      <ScreenView>
        <Text>Booking Screen</Text>
      </ScreenView>
    )
  }
}
