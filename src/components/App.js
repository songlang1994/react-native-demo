// @flow

import './Animations'

import { Navigation } from 'react-native-navigation'
import Screens from './Screens'

Navigation.startSingleScreenApp({
  screen: {
    screen: Screens.SignInSignUpScreen
  }
})
