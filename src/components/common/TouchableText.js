import React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import * as C from '../../constants/Colors'

import type { ViewStyleProp } from 'StyleSheet'

type TextSize = 'Small' | 'Medium' | 'Large'

type EventHandler = () => void

type Props = {
  onPress?: EventHandler,
  onLongPress?: EventHandler,
  style?: ViewStyleProp,
  children: string,
  textSize?: TextSize
}

export default function TouchableText(props: Props) {
  const textSize: TextSize = props.textSize || 'Small'
  const textStyle = [S.text, S[`text${textSize}`]]

  return (
    <TouchableOpacity onPress={props.onPress}
                      onLongPress={props.onLongPress}
                      activeOpacity={0.2}
                      style={[S.touchable, props.style]}>
      <Text style={textStyle}>
        {props.children}
      </Text>
    </TouchableOpacity>
  )
}

const S = StyleSheet.create({
  touchable: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  text: {
    padding: 5,
    textAlign: 'center',
    color: C.TEXT_LIGHT
  },

  textSmall: {
    fontSize: 14
  },

  textMedium: {
    fontSize: 18
  },

  textLarge: {
    fontSize: 22
  }
})