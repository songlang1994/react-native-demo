// @flow

import * as Animatable from 'react-native-animatable'

import * as Transitions from '../animations/Transitions'

Animatable.initializeRegistryWithDefinitions({
  ...Transitions
})
