// @flow

import MobileApi from './MobileApi'
import type { User } from '../Models'

type LoginParams = {
  email: string,
  password: string
}

type LoginSuccess = {
  success: true,
  data: User
}

type LoginFail = {
  success: false,
  code: LoginFailErrorCode
}

type LoginFailErrorCode = 'InvalidCredentials'

export async function login(params: LoginParams): Promise<LoginSuccess | LoginFail> {
  return MobileApi.post('login', params)
}
