import RestfulApi from './RestfulApi'
import { MOBILE_API_BASE } from '../constants/Settings'

export default new RestfulApi(MOBILE_API_BASE)
