// @flow

import { URLJoin } from '../utils/URLHelper'
import { camelizeKeys } from 'humps'

type QueryParams = {
  [k: string]: string
}

type Headers = {
  [k: string]: string
}

type Method = 'POST' | 'GET' | 'PUT' | 'DELETE'

export default class RestfulApi {
  baseURI = ''

  constructor(baseURI: string) {
    this.baseURI = baseURI
  }

  async get(path: string, params?: QueryParams, headers?: Headers) {
    const fullPath = URLJoin([this.baseURI, path], params)
    return this._request('GET', fullPath, headers)
  }

  async post(path: string, params?: {}, headers?: Headers) {
    const fullPath = URLJoin([this.baseURI, path])
    return this._request('POST', fullPath, headers, params)
  }

  async _request(method: Method, fullPath: string, headers?: Headers, body?: {}) {
    const options: Object = {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        ...headers
      }
    }
    if (body) {
      options.body = JSON.stringify(body)
    }
    try {
      const resp: Response = await fetch(fullPath, options)
      return camelizeKeys(await resp.json())
    } catch (e) {
      throw new Error(e.message)
    }
  }
}
