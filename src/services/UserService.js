// @flow

import { Platform } from 'react-native'
import MobileApi from './MobileApi'
import type { User } from '../Models'

type RegisterParams = {
  email: string,
  password: string
}

type RegisterSuccess = {
  success: true,
  data: User
}

type RegisterFail = {
  success: false,
  code: RegisterErrorCode
}

type RegisterErrorCode =
  | 'InvalidEmail'
  | 'InvalidPasswordFormat'

export async function register(params: RegisterParams): Promise<RegisterSuccess | RegisterFail> {
  return MobileApi.post('users', {
    user: {
      email: params.email,
      password: params.password,
      platform: Platform.OS
    }
  })
}
